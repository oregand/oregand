<img src="https://gitlab.com/uploads/-/system/user/avatar/1646689/avatar.png?width=400" alt="David O'Regan" width="128px" align="center" />

# HUG (Human User Guide)

## David O'Regan (I also go by dave, oregan, oregano, or whatever fun way you can turn my name into a spice)

- **Title**: Engineering Manager - Create:Editor, GitLab
- **Timezone**: TBA (this changes so often even I don't know)
- **Pronouns**: he/him/his
- **Myers–Briggs**: [ENTJ](https://www.16personalities.com/entj-personality)

## Connect
- **Meeting**: [Schedule Calendly](https://calendly.com/oregand)
- **LinkedIn**: [doregan](https://www.linkedin.com/oregand7/) 
- **Blog**: http://www.doregan.com


## Little Bit About Me

I love working out, maths, coffee, rap music (Eminem, Yelawolf, The Game), metal music, whiskey, and was a super [functional programming](https://github.com/MostlyAdequate/mostly-adequate-guide) nerd back in the day. I am attempting to learn to speak Spanish but failing at it pretty terribly. I am fantastic with numbers but bad with words which is unfortunate given I adore language and how we communicate. 

## How I Prefer To Work

- I like to stay up to date with software.
- I adore process automation.
- I specialize in people development.
- I hate bureaucracy.
- I work best with highly transparent people. 
- I encoruage crazy ideas and embracing failure.
- I enjoy pairing.

## How I Prefer To Communicate

**In order of best way to reach me**

- Slack me.
- @ me (I live in my Todos).
- Send a carrier pigeon with a bribe in zambian kwacha.
- Email me.

## Values

### Progress/Shipping/Delivery (Getting. Shit. Done.)

My first priority is always moving forward and getting things done. It's not that I think my other values aren't as important, but having come through my job history of a bunch of failed companies some consulting gigs and then a handful of sucess stories, the take away for me was if you aren't showing progress (shipping is one aspect, but also unshipping and making decisions) you don't have the opportunities and support to build great teams. 

### Extreme Ownership (because the regular kind often doesn't cut it)

Extreme Ownership is the practice of owning everything in your world, to an extreme degree. It means you are responsible for not just those tasks which you directly control, but for all those that affect whether or not your mission is successful. This is a core value of mine and something I promote with others as from my own experience it is ALWAYS better to lean into seeing what you can control in a situtation rather than what you cannot. Be the aggressor, not the victim. 

### Transparency

I value coming into a relationship with transparency and a genuine belief that we are all trying to do our best work and that even in disagreements they are done with the best of intent without maliciousness. Part of this transparency also comes between our teams and our leadership to deal with situations where the above is not true in a fair and swift manner. 

### Communication

I think poor communication tends to be the core of most problems. It is usually a result of fear (giving someone critical feedback that they won't like, not wanting to ask questions and feel stupid, etc) or when people don't take the time to verify that everyone is on the same page and are operating without a shared understanding.

### How I Prefer Giving Feedback

- Directly in 1-1's for constructive feedback.
- Publicly in issues, merges, sync meetings or in Slack for positive feedback. 

### How I Prefer Receiving Feedback

- Yes. (as often and as much as you like)

![values](https://about.gitlab.com/images/press/green-screens/gitlab-green-screen-1920x1080-2.png)

